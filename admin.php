<?php
session_start();
// Преобразование суперспособностей
function ability_string($a1, $a2, $a3, $a4) {
    $str = "";
    if ($a1 == 1) {
        $str .= "Бессмертие ";
    }
    if ($a2 == 1) {
        $str .= "Прохождение сквозь стены ";
    }
    if ($a3 == 1) {
        $str .= "Левитация ";
    }
    if ($a4 == 1) {
        $str .= "Невидимость";
    }
    return $str;
}

// Преобразование пола
function sex_string($sex) {
    if ($sex == 0) {
        $str = "Мужской";
    }
    else {
        $str = "Женский";
    }
    return $str;
}

// Преобразование соглашения
function accept_string($accept) {
    if ($accept == 1) {
        $str = "Принято";
    }
    return $str;
}

// Подключаемся к БД для получения данных из таблицы
include("bd.php");

// Получаем логин и пароль от учётной записи администратора
$query = $db->prepare("SELECT * FROM admin WHERE id = ?");
$query->execute([1]);
$row = $query->fetch();
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $row['login'] || md5($_SERVER['PHP_AUTH_PW']) != $row['pass']) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print('<h1><center>401 Требуется авторизация</center></h1>');
    exit();
}
print('<h3><b>Admin</b></h3><br>');

// echo $_SERVER['PHP_AUTH_USER'];
// echo $_SERVER['PHP_AUTH_PW'];
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
?>
<style>
/* Стили */
    table {
        border-radius: 10px;
        border-spacing: 0;
        text-align: center;
    }
    th {
        background: #706d97;
        color: white;
        text-shadow: 0 1px 1px #2D2020;
        padding: 5px 10px;
    }
    th, td {
        border-style: solid;
        border-width: 0 1px 1px 0;
        border-color: #FFFFFF;
    }
    th:first-child {
        border-top-left-radius: 8px;
    }
    th:last-child {
        border-top-right-radius: 10px;
        border-right: none;
    }
    tr {
        background-color: #FFFFFF;
    }
    tr:nth-child(odd) {
        background-color: #EEEEEE;
    }
    td {
        color: #696969;
        padding: 5px 10px;
    }
    tr:last-child td:first-child {
        border-radius: 0 0 0 10px;
    }
    tr:last-child td:last-child {
        border-radius: 0 0 10px 0;
    }
    tr, td:last-child {
        border-right: none;
    }
    .delete {
        text-decoration: none;
        display: inline-block;
        background: #EEEEEE;
        border-radius: 5px;
        color: black;
        padding: 5px;
    }
    .delete {
        text-align:center;
        font-size:13px;
        text-decoration: none;
        font-weight: 700;
        padding: 5px 10px;
        background: #eaeef1;
        background-image: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,.1));
        border-radius: 3px;
        color: rgba(0,0,0,.6);
        text-shadow: 0 1px 1px rgba(255,255,255,.7);
        box-shadow: 0 0 0 1px rgba(0,0,0,.2), 0 1px 2px rgba(0,0,0,.2), inset 0 1px 2px rgba(255,255,255,.7);
    }
    .delete:hover {
        background: #fff;
        cursor: pointer;
    }
    .delete:active {
        background: #d0d3d6;
        background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,0));
        box-shadow: inset 0 0 2px rgba(0,0,0,.2), inset 0 2px 5px rgba(0,0,0,.2), 0 1px rgba(255,255,255,.2);
    }
</style>

<a href="index.php">Выйти на главную страницу</a>
<br><br>

<form action="" method="POST">
<?php 

if (isset($_GET['del_id'])) {
    $stmt = $db->prepare("DELETE FROM users WHERE id = ?");
    $stmt->execute($_GET['del_id']);
}

$stmt = $db->prepare("SELECT * FROM users");
$stmt->execute();
// Шапка таблицы
$table = '
<table>
    <tr>
        <th>ID</th>
        <th>Имя</th>
        <th>email</th>
        <th>Год рождения</th>
        <th>Способности</th>
        <th>Пол</th>
        <th>Кол-во конечностей</th>
        <th>Текст</th>
        <th>Соглашение</th>
        <th>Логин</th>
        <th>Хэш пароля</th>
        <th>Удалить</th>
    </tr>
';
echo $table;
// Заполнение таблицы данными
while ($result = $stmt->fetch()) {
    echo "<tr>";
    echo "<td>". $result['id']."</td>";
    echo "<td>". $result['name']."</td>";
    echo "<td>". $result['email']."</td>";
    echo "<td>". $result['year']."</td>";
    echo "<td>". ability_string($result['immort'], $result['wall'], $result['levit'], $result['invis'])."</td>";
    echo "<td>". sex_string($result['sex'])."</td>";
    echo "<td>". $result['limbs']."</td>";
    echo "<td>". $result['text']."</td>";
    echo "<td>". accept_string($result['accept'])."</td>";
    echo "<td>". $result['login']."</td>";
    echo "<td>". $result['pass']."</td>";
    print '<td><input type="checkbox" name="check[]" value="'.$result["id"].'"></td>';
    echo "</tr>";
}
echo "</table>";
?>
<br><input type='submit' value='Удалить выделенные записи' name='delete' class='delete'>
</form>

<?php
// Обработка кнопки удаления
$ids_to_delete = array();
if (isset($_POST['delete'])) {
    // Переносим данные из полей в массив
    if (empty($_POST['check'])) {
        echo "";
    }
    else {
        $check = $_POST['check'];
        while (list ($key, $val) = @each ($check)) {
            $stmt = $db->prepare("DELETE FROM users WHERE id = ?");
            $stmt->execute([$val]);
        }
    }
    session_destroy();
    ?>
    <script type="text/javascript">
        window.location.href=window.location.href;
    </script>
    <?php
}
?>