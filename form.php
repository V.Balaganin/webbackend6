<?php header("Content-Type: text/html; charset=UTF-8");?>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form PHP</title>
	<style>
	   form {
	       width: 600px;
	       height: 700px;
	       background: white;
	       border-radius: 8px;
	       margin: 0 auto;
	       padding: 30px;
         padding-bottom: 60px;
	       box-shadow: 0px 0px 14px 0px rgba(46, 53, 55, 0.77);
	   }
	   p {
	       line-height: 0.5;
	   }
	   label {
	       margin: 3px;
	   }
	   input {
	       margin: 8px 0;
	   }
	   input[type="text"], input[type="email"] {
	       width: 100%;
	       height: 30px;
	       border-radius: 5px;
	       outline: none;
	       padding: 7px;
	   }
	   input[type="checkbox"] {
	       margin-right: 7px;
	   }
	   textarea {
	       width: 300px;
	       height: 150px;
	       padding: 7px;
	   }
	   input[type="submit"] {
	       padding: 7px 20px;
	       border-radius: 5px;
	       box-shadow: 0px 0px 5px 0px rgba(46, 53, 55, 0.5);
	   }
	   input[type="submit"]:hover {
	       cursor: pointer;
	   }
        .error {
            border: 1px solid red;
        }
        .connect {
            width: 100%;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 15px;
        }
        .connect a {
            margin: 0 5px;
        }
	</style>
</head>
<body>
     <div class="connect">
        <a href="reg.php">Зарегистрироваться</a>
        <a href="login.php">Войти</a>
        <a href="logout.php">Выйти из аккаунта</a>
        <a href="admin.php">Войти как администратор</a>
     </div>
     <?php
        session_start();
        if (empty($_SESSION['login']) or empty($_SESSION['id'])) {
            echo "<b>Вы вошли на сайт, как гость</b><br>";
        }
        else {
            echo "<b>Вы вошли на сайт, как ".$_SESSION['login']."</b><br>";
        }
        ?>
  <form action="" method="POST">
    <label>Ваше имя</label>
    <div>
    <input name="fio" type="text" <?php if ($errors['fio']) {print 'class="error"';}?> value="<?php print $values['fio'];?>">
    </div>
    <br>
    
    <label>Ваш email</label>
    <input name="email" type="text" <?php if ($errors['email']) {print 'class="error"';}?> value="<?php print $values['email'];?>">
    <br>
    
    <p>Год рождения</p>
    <select name="year">
    <?php for($i = 1900; $i < 2020; $i++) {?>
      <option value="<?php print $i; ?>"<?= $i == $values['year'] ? 'selected' : ""?>><?= $i;?></option>
      <?php }?>
      <?php if ($errors['year']) {print 'class="error"';}?>
    </select>
    <br>
    
    <p>Пол</p>
    <label class="radio">
      <input type="radio" name="sex" value="0" checked <?php if($_COOKIE['sex']){ echo 'checked="checked"';}?>>
      Мужской
    </label>
    <label class="radio">
      <input type="radio" name="sex" value="1" <?php if($_COOKIE['sex']){ echo 'checked="checked"';}?>>
      Женский
    </label>
    <br>
    
    <p>Количество конечностей</p>
    <label class="radio">
      <input type="radio" name="limbs" value="2" checked <?php echo $values['limbs'] == "2" ? 'checked="checked"' :""?>>
      2
    </label>
    <label class="radio">
      <input type="radio" name="limbs" value="4" <?php echo $values['limbs'] == "4" ? 'checked="checked"' :""?>>
      4
    </label>
    <label class="radio">
      <input type="radio" name="limbs" value="6" <?php echo $values['limbs'] == "6" ? 'checked="checked"' :""?>>
      6
    </label>
    <label class="radio">
      <input type="radio" name="limbs" value="8" <?php echo $values['limbs'] == "8" ? 'checked="checked"' :""?>>
      8
    </label>
    <label class="radio">
      <input type="radio" name="limbs" value="10" <?php echo $values['limbs'] == "10" ? 'checked="checked"' :""?>>
      10
    </label>
    <br>
    <br>
    
    <select name="abilities[]" multiple <?php if ($errors['abilities']) {print 'class="error"';}?>>
      <?php 
      foreach ($abilities as $key => $value) {
        $selected = !empty($values['abilities'][$key]) ? "" : 'selected="selected"';
        printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
      }
      ?>
    </select>
    <br>
    
    <p>Ваша краткая биография</p>
    <textarea name="text" placeholder="Ваша биография" rows=10 cols=30 <?php if ($errors['text']) {print 'class="error"';}?>><?php print $values['text'];?></textarea>
    <br>
    
    <input type="checkbox" name="accept" <?php if ($errors['accept']) {print 'class="error"';}?> <?= $values['accept'] == "on" ? 'checked="checked"' : "";?>>Принимаю
    <br>
    <input type="submit" value="Отправить">

  </form>
</body>
</html>
