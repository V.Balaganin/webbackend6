<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Массив суперспособностей
$abilities = array(
    'immort' => "Бессмертие",
    'wall' => "Прохождение сквозь стены",
    'levit' => "Левитация",
    'invis' => "Невидимость");

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
// Выдаём форму методом GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 1);
        setcookie('login', '', 1);
        setcookie('pass', '', 1);
        // Выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['abilities'] = !empty($_COOKIE['abilities_error']);
    $errors['text'] = !empty($_COOKIE['text_error']);
    $errors['accept'] = !empty($_COOKIE['accept_error']);

    // Выдаем сообщения об ошибках
    // ФИО
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('fio_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['fio_error'] == "1") {
            $messages[] = '<div class="error">Заполните имя</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректное имя</div>';
        }
    }

    // email
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('email_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['email_error'] == "1") {
            $messages[] = '<div class="error">Заполните email</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректный email</div>';
        }
    }

    // Год рождения
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('year_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['year_error'] == "1") {
            $messages[] = '<div class="error">Заполните год</div>';
        }
        else {
            $messages[] = '<div class="error">Укажите корректный год</div>';
        }
    }
    
    // Принять
    if ($errors['accept']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('accept_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['accept_error'] == "1") {
            $messages[] = '<div class="error">Вы не приняли соглашение</div>';
        }
    }

    // Биография
    if ($errors['text']) {
        // Удаляем куку, указывая время устаревания в прошлом
        setcookie('text_error', '', 1);
        // Выводим сообщение
        if ($_COOKIE['text_error'] == "1") {
            $messages[] = '<div class="error">Заполните текстовое поле</div>';
        }
    }

    // Способности
    if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом
      setcookie('abilities_error', '', 1);
      // Выводим сообщение
      if ($_COOKIE['abilities_error'] == "1") {
          $messages[] = '<div class="error">Выберите споособность</div>';
      }
      else {
          $messages[] = '<div class="error">Выбрана недопустимая способность</div>';
      }
    }


    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['sex'] = $_COOKIE['sex_value'] === '0' ? 0 : 1;
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    if (!empty($_COOKIE['abilities_value'])) {
        $abilities_value = json_decode($_COOKIE['abilities_value']);
    }
    $values['abilities'] = array();
    if (is_array($abilities_value)) {
        foreach($abilities_value as $ability) {
            if (!empty($abilities[$ability])) {
                $values['abilities'][$ability] = $ability;
            }
        }
    }
    $values['text'] = empty($_COOKIE['text_value']) ? '' : $_COOKIE['text_value'];
    $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
        // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода 
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST
// Обрабатываем сохранения
else {
    // Валидация формы
    $errors = FALSE;

    // ФИО
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_POST['fio'])) {
        setcookie('fio_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        }
        // Сохраняем ранее введенное в форму значение на год
        setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // email
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле email.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[^@]+@[^@.]+\.[^@]+$/', $_POST['email'])) {
        setcookie('email_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        }
        // Сохраняем ранее введенное в форму значение на год
        setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // Год рождения
    if (empty($_POST['year'])) {
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        $year = $_POST['year'];
        if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) {
        setcookie('year_error', '2', time() + 24 * 60 * 60);  
            $errors = TRUE;
        }
        setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // Пол
    setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);

    // Конечности
    if (empty($_POST['limbs'])) {
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год
        setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // Способности
    if (empty($_POST['abilities'])) {
        setcookie('abilities_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        $abilities_error = FALSE;
        foreach($_POST['abilities'] as $key) {
            if (empty($abilities[$key])) {
                setcookie('abilities_error', '2', time() + 24 * 60 * 60);
                $errors = TRUE;
                $abilities_error = TRUE;
            }
        }
        if (!$abilities_error) {
            setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
        }
    }

    // Биография
    if (empty($_POST['text'])) {
        setcookie('text_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('text_value', $_POST['text'], time() + 12 * 30 * 24 * 60 * 60);
    }

    // Принять
    if (!isset($_POST['accept'])) {
        setcookie('accept_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('accept_value', $_POST['accept'], time() + 12 * 30 * 24 * 60 * 60);
    }

    $ability_data = ['immort', 'wall', 'levit', 'invis'];
    $abil = $_POST['abilities'];
    // print($values['abilities']);
    foreach($ability_data as $ability) {
        $ability_insert[$ability] = in_array($ability, $abil) ? 1 : 0;
    }

    // Проверяем массив ошибок
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 1);
        setcookie('email_error', '', 1);
        setcookie('year_error', '', 1);
        setcookie('sex_error', '', 1);
        setcookie('limbs_error', '', 1);
        setcookie('abilities_error', '', 1);
        setcookie('text_error', '', 1);
        setcookie('accept_error', '', 1);
    }

    // Соединение с БД
    include("bd.php");

    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    // Пользователь уже известен
    if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
        $stmt = $db->prepare("UPDATE users SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, immort = ?, wall = ?, levit = ?, invis = ?, text = ?, accept = ? WHERE login = ?");
        $stmt->execute(array($_POST['fio'], $_POST['email'], intval($_POST['year']), intval($_POST['sex']), intval($_POST['limbs']), $ability_insert['immort'], $ability_insert['wall'], $ability_insert['levit'], $ability_insert['invis'], $_POST['text'], 1, $_SESSION['login']));
    }
    // Новый пользователь
    else {
        exit("Для того, чтобы отправить форму, нужно зарегистрироваться!");
    }
    
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    // Делаем перенаправление.
    header('Location: ./');
}
