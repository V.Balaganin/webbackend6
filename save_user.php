<?php 
/* Файл, который будет заносить данные в базу и сохранять пользователя */
// Массив для хранения ошибок
$err = array();
// Заносим введенный пользователем логин в переменную $login, если он пустой,
// то уничтожаем переменную $login
if (isset($_POST['login'])) {
    $login = $_POST['login'];
    if ($login == '') {
        unset($login);
    }
}
// То же самое делаем с паролем
if (isset($_POST['pass'])) {
    $pass = $_POST['pass'];
    if ($pass == '') {
        unset($pass);
    }
}
//если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
if (empty($login) || empty($pass)) {
    exit("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
}
// Проверка валидности логина
if (!preg_match("/^[a-zA-Z0-9]+$/", $login)) {
    $err['login'] = "Логин может состоять только из букв английского алфавита и цифр";
}
if (strlen($login) < 4 or strlen($login) > 20) {
    $err['login2'] = "Логин должен быть не меньше 4-х символов и не больше 20";
}
// Проверка валидности пароля
if (strlen($pass) < 8) {
    $err['pass'] = "Длина пароля должна быть не меньше 8 символов";
}

if (count($err) == 0) {
    //если логин и пароль введены, то обрабатываем их
    $login = trim($login);
    $pass = md5(trim($pass));

    // Подключаемся к базе данных
    include("bd.php");

    // Проверяем, есть ли пользователь в БД с таким логином
    $query = $db->prepare("SELECT id FROM users WHERE login = ?");
    $query -> execute([$login]);
    $row = $query->fetch();
    if (!empty($row['id'])) {
        exit("Извините, введённый вами логин уже зарегистрирован. Введите другой логин.");
    }
    //!empty($row['id']
    //$query->rowCount() > 0
    // Если такого логина в БД нет, то сохраняем данные в БД
    $query2 = $db->prepare("INSERT INTO users (login, pass) VALUES (:login, :pass)");
    $query2->bindParam(':login', $login);
    $query2->bindParam(':pass', $pass);
    $query2->execute();

    // Проверяем наличие ошибок
    if ($query2) {
        echo "Вы успешно зарегистрированы! Теперь вы можете зайти на сайт. <a href='login.php'>Страница входа</a>";
    }
    else {
        echo "Ошибка! Вы не зарегистрированы.";
    }
}
else {
    echo "<b>При регистрации произошли следующие ошибки:</b><br>";
    foreach($err as $error) {
        echo $error."<br>";
    }
}

?>